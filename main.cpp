#include <QApplication>
#include <FelgoApplication>

#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "precompiled.h"
#include "drug.h"
#include <QxMemLeak.h>

// uncomment this line to add the Live Client Module and use live reloading with your custom C++ code
//#include <FelgoLiveClient>

void setup() {
    // Parameters to connect to database
    qx::QxSqlDatabase::getSingleton()->setDriverName("QSQLITE");
    qx::QxSqlDatabase::getSingleton()->setDatabaseName("./mydata.db");
    qx::QxSqlDatabase::getSingleton()->setHostName("localhost");
    qx::QxSqlDatabase::getSingleton()->setUserName("root");
    qx::QxSqlDatabase::getSingleton()->setPassword("");

    // Create table 'drug' into database to store drugs
//    QSqlError daoError = qx::dao::create_table<drug>();

    // Create 3 new drugs
    // It is possible to use 'boost' and 'Qt' smart pointer :
    // 'boost::shared_ptr', 'QSharedPointer', etc...
    typedef std::shared_ptr<drug> drug_ptr;
    drug_ptr d1; d1.reset(new drug()); d1->name = "name1"; d1->description = "desc1";
    drug_ptr d2; d2.reset(new drug()); d2->name = "name2"; d2->description = "desc2";
    drug_ptr d3; d3.reset(new drug()); d3->name = "name3"; d3->description = "desc3";

    // Insert drugs into container
    // It is possible to use a lot of containers from
    // 'std', 'boost', 'Qt' and 'qx::QxCollection<Key, Value>'
    typedef std::vector<drug_ptr> type_lst_drug;
    type_lst_drug lst_drug;
    lst_drug.push_back(d1);
    lst_drug.push_back(d2);
    lst_drug.push_back(d3);

    // Insert drugs from container to database
    // 'id' property of 'd1', 'd2' and 'd3' are auto-updated
    qx::dao::insert(lst_drug);

}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    FelgoApplication felgo;

    // Use platform-specific fonts instead of Felgo's default font
    felgo.setPreservePlatformFonts(true);

    QQmlApplicationEngine engine;
    felgo.initialize(&engine);

    // Set an optional license key from project file
    // This does not work if using Felgo Live, only for Felgo Cloud Builds and local builds
    felgo.setLicenseKey(PRODUCT_LICENSE_KEY);

    // use this during development
    // for PUBLISHING, use the entry point below
    felgo.setMainQmlFileName(QStringLiteral("qml/Main.qml"));

    // use this instead of the above call to avoid deployment of the qml files and compile them into the binary with qt's resource system qrc
    // this is the preferred deployment option for publishing games to the app stores, because then your qml files and js files are protected
    // to avoid deployment of your qml files and images, also comment the DEPLOYMENTFOLDERS command in the .pro file
    // also see the .pro file for more details
    // felgo.setMainQmlFileName(QStringLiteral("qrc:/qml/Main.qml"));

    setup();

    qx::IxModel * pModel = new qx::QxModel<drug>();
    pModel->qxFetchAll();
    qDebug() << "rowCount " << pModel->rowCount();

    engine.rootContext()->setContextProperty("drug", pModel);
    engine.load(QUrl(felgo.mainQmlFileName()));

    // to start your project as Live Client, comment (remove) the lines "felgo.setMainQmlFileName ..." & "engine.load ...",
    // and uncomment the line below
    //FelgoLiveClient client (&engine);

    return app.exec();
}

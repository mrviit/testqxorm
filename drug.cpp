#include "precompiled.h"   // Precompiled-header with '#include <QxOrm.h>'
                           // and '#include "export.h"'
#include "drug.h"          // Class definition 'drug'
#include <QxMemLeak.h>     // Automatic memory leak detection

QX_REGISTER_CPP_EXPORT_DLL(drug)   // This macro is necessary to register
                                    // 'drug' class in QxOrm context

//QX_REGISTER_ALL_QT_PROPERTIES(drug, "id")

namespace qx {
template <> void register_class(QxClass<drug> & t)
{
    //    qx::register_all_qt_properties<drug>(t, "id");

    t.id(& drug::id, "id");               // Register 'drug::id' <=> primary key in your database
    t.data(& drug::name, "name", 1);      // Register 'drug::name' property with key 'name' and version '1'
    t.data(& drug::description, "dut");  // Register 'drug::description' property with key 'desc'
}}


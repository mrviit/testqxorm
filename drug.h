#ifndef _CLASS_DRUG_H_
#define _CLASS_DRUG_H_

class drug
{
public:
    long id;
    QString name;
    QString description;

    drug() : id(0) { ; }
    virtual ~drug() { ; }
};

QX_REGISTER_HPP_EXPORT_DLL(drug, qx::trait::no_base_class_defined, 1)

/* This macro is necessary to register 'drug' class in QxOrm context */
/* param 1 : the current class to register => 'drug' */
/* param 2 : the base class, if no base class, use the qx trait =>
/* 'qx::trait::no_base_class_defined' */
/* param 3 : the class version used by serialization to provide 'ascendant compatibility' */

#endif // _CLASS_DRUG_H_
